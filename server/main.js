import { Meteor } from 'meteor/meteor';
import Movies from  '../imports/db/movies/collection';
import '../imports/db/movies/ratings/collection';
import '../imports/db/movieComments/collection';




Meteor.startup(() => {
  // If the Links collection is empty, add some data.
  if (Movies.find().count() === 0) {
    Movies.insert({
      title:'John Wick 3',
      image:"https://amc-theatres-res.cloudinary.com/image/upload/f_auto,fl_lossy,h_300,q_auto,w_200/v1554742347/amc-cdn/production/2/movies/55000/54963/PosterDynamic/75363.jpg",
      description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis, consequatur. Quasi saepe illo, velit distinctio aperiam voluptates dolore iusto ad incidunt quisquam voluptas voluptate atque obcaecati reiciendis culpa optio officia!'
    });

    Movies.insert({
      title:'Aladino',
      image:"https://amc-theatres-res.cloudinary.com/image/upload/f_auto,fl_lossy,h_300,q_auto,w_200/v1557783190/amc-cdn/production/2/movies/60200/60212/PosterDynamic/76531.jpg",
      description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis, consequatur. Quasi saepe illo, velit distinctio aperiam voluptates dolore iusto ad incidunt quisquam voluptas voluptate atque obcaecati reiciendis culpa optio officia!'
    })
    console.log("no hay");
  }
});
