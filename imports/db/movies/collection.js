import { Mongo } from 'meteor/mongo';
import MovieSchema from './schema';
import Ratings from './ratings/collection';
import Comments from '../movieComments/collection';

import { ReactiveAggregate } from 'meteor/jcbernack:reactive-aggregate';

const Movies = new Mongo.Collection('movies');

Movies.attachSchema(MovieSchema);

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("movies", function () {
    ReactiveAggregate(this, Movies, [
      {
        $lookup: {
          from: Comments,
          localField: '_id',
          foreignField: 'movie',
          as: 'comments'
        }
      },
      {
      $lookup: {
          from: Ratings,
          localField: '_id',
          foreignField: 'movie',
          as: 'ratings'
        }
    }
  ]);
});
 
}


export default Movies;