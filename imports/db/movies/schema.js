import SimpleSchema from 'simpl-schema';
export default new SimpleSchema({
    title: {
        type: String,
    },
    image:{
      type:String
    },
    description:{
      type:'string'
    }
})