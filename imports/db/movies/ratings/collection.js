import { Mongo } from 'meteor/mongo';
import RatingSchema from './schema';
import { check } from 'meteor/check';

const Ratings = new Mongo.Collection('ratings');
Ratings.attachSchema(RatingSchema);


Meteor.methods({
  'ratings.rateMovie'(movie, rating ) {
    check(rating, Number);

    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    let movieRated =  Ratings.findOne({owner:this.userId, movie});

    if(movieRated){
      Ratings.update(movieRated._id, { $set: { rating: rating } });
    }else{
      Ratings.insert({
        rating,
        movie,
        createdAt: new Date(),
      });
    }
  },
});

if (Meteor.isServer) {
  Meteor.publish('ratings', function() {
    return Ratings.find();
  });
}


export default Ratings;