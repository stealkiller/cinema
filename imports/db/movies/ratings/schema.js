import SimpleSchema from 'simpl-schema';
export default new SimpleSchema({
    movie:{
      type: String,
      regEx: SimpleSchema.RegEx.Id
    },
    rating:{
      type:Number
    },
    owner:{
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      autoValue(){
        return this.userId;
      }
    },
})