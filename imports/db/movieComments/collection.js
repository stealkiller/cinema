import { Mongo } from 'meteor/mongo';
import CommentSchema from './schema';
import { check } from 'meteor/check';

const Comments = new Mongo.Collection('comments');

Comments.attachSchema(CommentSchema);

Meteor.methods({
  'comments.makeComment'(movie, comment ) {
    check(comment, String);

    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Comments.insert({
      comment,
      movie,
      createdAt: new Date(),
    });
  },
});



export default Comments;