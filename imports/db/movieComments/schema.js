import SimpleSchema from 'simpl-schema';
export default new SimpleSchema({
    comment:{
      type: String
    },
    owner:{
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      autoValue(){
        return this.userId;
      }
    },
    username:{
      type: String,
      autoValue(){
        return Meteor.user().username
      }
    },
    movie:{
      type:String,
      regEx: SimpleSchema.RegEx.Id
    }
})