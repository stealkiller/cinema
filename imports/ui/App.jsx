import React from 'react';
import MenuApp from './components/MenuApp';
import Section from './components/Section';

const App = () => (
  <div className="wrapper-app">
     <MenuApp/>
     <main>
        <Section/>
     </main>
  </div>
 
);

export default App;
