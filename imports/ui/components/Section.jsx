import React, { Component } from 'react';
import Slider from "react-slick";
import Movie from './Movie';
import { withTracker } from 'meteor/react-meteor-data';
import Movies from '../../db/movies/collection' 
import ModalContent from './ModalContent';
import Modal from 'react-modal';



Modal.setAppElement('#react-target')
Modal.defaultStyles.overlay.backgroundColor = 'rgba(0,0,0,0.75)';

class Section extends Component {

  constructor(props){
    super();
    this.state = {
      modalIsOpen: false,
      movieSelected:null
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(movie) {
    this.setState({
      modalIsOpen: true,
      movieSelected:movie
    });

    console.log(this.state)
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  renderMovies(){
   
    return this.props.movies.map((movie,index)=>(
      <Movie key={movie._id}  movie={movie} openModal={()=>this.openModal(index)}  />
    ))
  }
 
  
  render() {
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      
    };
    return (
      <div>
        <div className="section">
          <h1>Ultimas peliculas </h1>

          <Slider className="movie-list" {...settings}>
            {this.renderMovies()}
          </Slider>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          // onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <div className="modal-header" style={customStyles.modalHeader}>
            <button onClick={this.closeModal}>x</button>
          </div>
          <ModalContent movie={this.props.movies[this.state.movieSelected]} currentUser={this.props.currentUser} />
        </Modal>
      </div>
    );
  }
}

const customStyles = {
  content : {
   padding:'0px'
  },
  modalHeader:{
    display: 'flex',
    justifyContent: 'flex-end'
  }
};


export default withTracker(() => {
  Meteor.subscribe('movies');
  return {
    movies: Movies.find({}).fetch(),
   //movies: Movies.createQuery(), 
   currentUser: Meteor.user(),
  };
})(Section);


