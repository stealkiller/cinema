import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class ModalContent extends Component {
  constructor(props){
    super(props);
  }

  makeComment(){

    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

    Meteor.call('comments.makeComment', this.props.movie._id, text);

    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }

  showCommentBox(){
    if(this.props.currentUser){
      return (
        <div style={Styles.commentContent}>
        <div className="comment-area">
          <textarea style={Styles.commentBox}   rows="3" ref="textInput"></textarea>
        </div>
           <div className="send-comment">
              <button onClick={()=>this.makeComment()}>Comment</button>
            </div>
       </div>
      )
    }
  }

  renderComments(){
    return this.props.movie.comments.map((current)=>(
      <Comment key={current._id} current={current} />
    ));
  }

  render() {
    return (
     <div style={Styles.contentModal}>
       <div style={Styles.modalImage} className="modal-image"> 
        <img src={this.props.movie.image} alt=""/>
       </div>
       <div style={Styles.movieTitle} className="movie-title">
        {this.props.movie.title}
       </div>

       <div style={Styles.description} className="description">
        {this.props.movie.description}
       </div>
        {this.showCommentBox()}
       <div className="commentList">
        {this.renderComments()}
       </div>
     
     </div>
    );
  }
}

function Comment(props){
  console.log(props, 'esta son las propiedades');
  return (
    <div className="comment">
      <p><strong>{props.current.username}:</strong> {props.current.comment}</p>
    </div>
  )
}


const Styles = {
  contentModal:{
    color:'black',
    padding: '20px'
  },
  modalImage:{
    display:'flex',
    justifyContent:'center'
  },
  movieTitle:{
    fontSize:15,
    fontWeight:'bold',
  },
  description:{

  },
  commentContent:{
    display:'grid',
    gridTemplateRows:'1fr auto'
  },
  commentBox:{
    width: '100%',
    resize: 'none'
  }
};
