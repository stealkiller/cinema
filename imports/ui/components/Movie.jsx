import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';
import { withTracker } from 'meteor/react-meteor-data';

 class Movie extends Component {

  constructor(props){
    super(props);

    this.state = {
      rating: 1,
      editing:false
    };
  }

  onStarClick(nextValue) {
    this.setState({rating: nextValue});
    Meteor.call('ratings.rateMovie', this.props.movie._id, nextValue);
  }

  getAverage(ratings){
    let length = ratings.length;
    let sum = ratings.reduce(function(total,current){
      return total + current.rating;
    }, 0);
    
    return sum/length;
  }


  render() {
    return (
      <div className="movie-item">
        <div className="movie-image">
          <img src={this.props.movie.image} alt=""/>
          <div className="rating">
            <StarRatingComponent 
              name="rate2" 
              editing={!!this.props.currentUser}
              onStarClick={this.onStarClick.bind(this)}
              starCount={10}
              value={this.getAverage(this.props.movie.ratings)}
          />
          </div>
        </div>
        <div className="movie-title">
          {this.props.movie.title}
        </div>
        <div className="button-movie">
          <button onClick={this.props.openModal}>View details</button>
        </div>
    </div>
    );
  }
}

export default withTracker(() => {
  return {
    currentUser: Meteor.user(),
  };
})(Movie);

