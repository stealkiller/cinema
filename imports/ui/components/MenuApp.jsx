import React, { Component } from 'react';
import AccountsUIWrapper from './AccountsUIWrapper';
export default class MenuApp extends Component {


  render() {
    return (
     <header className="header">
     <div className="logo">
      <img src="https://image.flaticon.com/icons/png/512/732/732228.png" alt=""/>
     </div>
       <ul className="menu-list">
         <li>Inicio</li>
         <li>Cartelera</li>
       </ul>
       <div className="login-area">
        <AccountsUIWrapper/>
       </div>
     </header>
    );
  }
}
